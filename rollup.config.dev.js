import resolve from '@rollup/plugin-node-resolve';//一个Rollup插件,使用Node解析算法定位模块,用于使用node_modules中的第三方模块
import commonjs from '@rollup/plugin-commonjs';//一个Rollup插件,可以将CommonJS模块转换为ES6,这样它们就可以包含在Rollup bundle中
import typescript from 'rollup-plugin-typescript2';
import serve from 'rollup-plugin-serve';
import livereload from "rollup-plugin-livereload";// 必须要搭配 rollup --config rollup.config.dev.js -w 的 `-w` 参数

export default {
  input: 'src/index.ts',
  output: {
    file: 'lib/bundle.umd.js',//输出文件的路径和名称

    format: 'umd',//六种输出格式：amd/es6/iife/umd/cjs/system

    name: '$u'//当format为iife和umd时必须提供,将作为全局变量挂在window下
  },
  plugins: [
    resolve(),
    commonjs(),
    typescript(),
    serve({
      open: true,
      port: 9898,
      contentBase: './'
    }),
    livereload()
  ]
}
