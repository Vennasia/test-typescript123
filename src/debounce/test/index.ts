import debounce from "../index";

const fn = ()=>console.log(123);
const waitTime = 100;

/** case1: 测试基本功能*/
const fnx = debounce(fn, waitTime, false);

fnx()
fnx()
fnx()
fnx()
//123

setTimeout(()=>{
  fnx()
},waitTime+100)
//123

/** case2: 测试immediate*/
const fnx2 = debounce(fn, waitTime, true);
fnx2();
//123
//123
