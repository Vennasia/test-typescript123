declare const _default: {
    debounce: (func: Function, wait: number, immediate?: boolean | undefined) => Function;
};
export default _default;
