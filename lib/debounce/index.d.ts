/**
 * 防抖
 * @param func      需要被防抖的函数
 * @param wait      真正执行要触发的函数所需要等待的毫秒数
 * @param immediate 为true时,会立即执行一次,然后再加入到setTimeout里,也就是说会执行两次
 */
declare const debounce: (func: Function, wait: number, immediate?: boolean | undefined) => Function;
export default debounce;
